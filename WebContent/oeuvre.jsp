<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

		<% String title = "ajouter une oeuvre";
		   String body  = "ajout_oeuvre"; %>
		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp" %>
		
            <div id="content">
                <div class="contentbg">
                    <div class="post">
                        <div id="Div1">
                            <h1>Ajouter une oeuvre</h1>
                            <form class="formulaire" action="formdemo.php" method="post">
                            <select name="categorie" size="1">
                                <option>Livre</option>
                                <option>CD</option>
                                <option>DVD</option>
                            </select>
                            <label for="titre">Titre</label>
                            <input type="text" name="titre" required placeholder="Titre" />
                            <label for="name">Auteur</label>
                            <input type="text" name="auteur" placeholder="Auteur" />
                            <label for="text">Editeur</label>
                            <input type="text" name="editeur" placeholder="Editeur" />
                            <label for="text">ISBN</label>
                            <input type="text" name="isbn" placeholder="ISBN" />
                            <label for="text">Année</label>
                            <input type="text" name="annee" placeholder="annee" />
                            <label for="text">Genre</label>
                            <input type="text" name="genre" placeholder="genre" />
                            <input type="submit" value="Ajouter" />
                            </form>
                        </div>
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            <!-- end #content -->
            <div id="sidebar-bg">
                <div id="sidebar">
                    <ul>
                        <li>
                            <h2>Ajout oeuvre</h2>
                            <p>
                                Ajoute une oeuvre dans la base de donnée.
                            </p>
                        </li>
                        <li>
                            <h2>Autres Actions</h2>
                            <ul>
                                <li><a href="exemplaire.html">Ajout d'un exemplaire</a></li>
                                <li><a href="ajout_adherent.html">Ajout d'un adhérent</a></li>
                            </ul>
                         </li>
                    </ul>
                </div>
            </div>
            <!-- end #sidebar -->
            <div style="clear: both;">&nbsp;</div>

<%@ include file="include/footer.html" %>
