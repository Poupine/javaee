<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,entities.biblion7EE.*"%>

		<% String title = "Résultats de la recherche";
		   String body  = "resultats"; %>
		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp" %>
		
		
		<%@ include file="include/sidebar.jsp" %>		
		<div id="page">

			<% ArrayList<Oeuvre> toAdd = new ArrayList<Oeuvre>();
			   int nb_empruntables = 0;%>

            <div id="content">
                <div class="contentbg">
                <% if (cart != null) {%>
                	<form method="post" action="GestionPanier">
                <% } %>
                	<table id="newspaper-a">
                		<caption> <strong> Résultats </strong> </caption>
	    				<thead>
						      <tr>
						      	<th scope="col"> Titre de l'oeuvre </th>
						      	<th scope="col"> Auteur / Réalisateur / Chanteur  </th>
						      	<th scope="col"> Editeur / Producteur / Compositeur </th>
						      	<th scope="col"> Année de parution </th>
						      	<th scope="col"> Disponibilité </th>
						      	<th scope="col"> Genre </th>
						      	<th scope="col"> Catégorie </th>
						      	<th scope="col">
							      	<%if (cart != null) { %>
								      	 Emprunter
								    <%} %>
							    </th>
							  </tr>
						 </thead>
                    <% 
					ArrayList<Oeuvre> resultats = (ArrayList<Oeuvre>)application.getAttribute("resultats");
                    if (resultats == null) {
                    	response.sendRedirect("");
                    }
                    else if (resultats.size()==0) {%>
						<tbody>
							<tr>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
								<td>-</td>
							</tr>
						</tbody>
                    <%} else {
						int cpt_opt = 0;
						for(Iterator i = resultats.iterator(); i.hasNext();) {
							Oeuvre oeuvre = (Oeuvre)i.next();
					%>
					    <tbody>
					      <tr>
					      	<td><a href="GestionAffichageOeuvre?auteur=<%=oeuvre.getAuteur()%>&titre=<%=oeuvre.getTitre()%>"><%=oeuvre.getTitre() %></a></td>
					      	<td> <%=oeuvre.getAuteur() %> </td>
					      	<%if (oeuvre instanceof DVD) { %>
					      		<td> <% out.print(((DVD)oeuvre).getProducteur()); %> </td>
					      	<% } else if (oeuvre instanceof Livre){ %>
					      		<td> <% out.print(((Livre)oeuvre).getEditeur()); %> </td>
					      	<% } else if (oeuvre instanceof CD){ %>
					      		<td> <% out.print(((CD)oeuvre).getCompositeur()); %> </td>
					      	<%} %>
					      	<td> <%=oeuvre.getAnneeParution() %> </td>
					      	<td> <%=oeuvre.getNb_exemplaires() %> </td>
					      	<td> <%=oeuvre.getGenre() %> </td>
							<%if (oeuvre instanceof DVD) { %>
					      	<td> DVD </td>
					      	<% } else if (oeuvre instanceof Livre){ %>
					      	<td> Livre </td>
					      	<% } else if (oeuvre instanceof CD){ %>
					      	<td> CD </td>
					      	<%} %>
					      	<td> 
					      	<%if (cart != null && oeuvre.getNb_exemplaires() > 0) {%>
					      			<input type="checkbox" name="option" value="<%= cpt_opt++%>">
					      			<% toAdd.add(oeuvre);
					      			  nb_empruntables += 1; %>
					      	<%} %>
					      	</td>
					      </tr>
					     </tbody>
					<% 	} %>
						<%if (cart != null && resultats != null) { %>
						<tbody>
						<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>
							<% currentSession.setAttribute("addToCart", toAdd);
							   if (nb_empruntables > 0) {
							%>
							
							<input class="button" type="submit" name="action" value="Ajouter au panier" />
							<% } %>
						</td>
						</tr>
						</tbody>
						<%} %>
                    <%}
					%>
					</table>
					<%if (cart != null) {%>
						</form>
					<%} %>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            <!-- end #content -->
            

	</div>
	<!-- end #page -->
