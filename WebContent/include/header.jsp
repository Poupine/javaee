<%@page import="javax.naming.InitialContext"%>
<%@page import="javax.ejb.EJB"%>
<%@page import="java.text.*"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<% if (title == "")
		title = "Biblion7EE";
   else
		title = "Biblion7EE - " + title;
%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title><%= title %></title>
<link href="http://fonts.googleapis.com/css?family=Abel"
	rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/formulaire.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/design.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/tableaux.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="javascript/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="javascript/jquery.slidertron-1.0.js"></script>
<script type="text/javascript" src="javascript/jquery.dropotron-1.0.js"></script>
<script type="text/javascript" src="javascript/jquery.poptrox-1.0.js"></script>
<script type="text/javascript" src="javascript/calendrier.js"></script>
</head>
<body id="<%= body %>">
	<div id="wrapper">
		<div id="header-wrapper">
			<div id="header">
				<div id="logo">
					<div id="login">
						<%@ include file="connexion.jsp" %>
					</div>
					<h1>Biblion7EE</h1>
					<p>Projet JavaEE 2011/2012</p>
				</div>
			</div>
		</div>
	</div>
	<!-- end #header -->
	
	
