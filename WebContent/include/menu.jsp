
<%@page import="entities.biblion7EE.Oeuvre"%>
<%@page import="java.util.HashSet"%>
<%@page import="entities.biblion7EE.Adherent"%>
<%@page import="entities.biblion7EE.Administrateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

        <div id="menu-wrapper">
           <ul id="menu">
			<li><a id="index" href="index.jsp"><span>Acceuil</span></a></li>
            <li><a id="recherche" href="recherche.jsp"><span>Recherche</span></a></li>
			<li><span id="livre">Livres</span>
				<ul>
				    <li class="first"> <a href="Recherche?categorie=livre">Tous</a> </li>
					<li> <a href="Recherche?categorie=livre&genre=classique">Classiques</a> </li>
					<li> <a href="Recherche?categorie=livre&genre=fantasy">Fantasy</a> </li>
					<li class="last"> <a href="Recherche?categorie=livre&genre=policiers">Policiers</a> </li>
				</ul>
			</li>
            <li><span id="dvd">DVD</span>
				<ul>
					<li class="first"> <a href="Recherche?categorie=dvd">Tous</a> </li>
					<li> <a href="Recherche?categorie=dvd&genre=action">Action</a> </li>
					<li> <a href="Recherche?categorie=dvd&genre=western">Western</a> </li>
					<li class="last"> <a href="Recherche?categorie=dvd&genre=anticipation">Anticipation</a> </li>
				</ul>
			</li>
	        <li><span id="cd">CD</span>
				<ul>
					<li class="first"> <a href="Recherche?categorie=cd">Tous</a> </li>
					<li> <a href="Recherche?categorie=cd&genre=classique">Classique</a> </li>
					<li> <a href="Recherche?categorie=cd&genre=pop">Jpop</a> </li>
					<li class="last"> <a href="Recherche?categorie=cd&genre=rock">Rock</a> </li>
				</ul>
			</li>
			<%
			if (currentSession.getAttribute("session") != null) {
				Adherent adh = (Adherent)currentSession.getAttribute("session");
			%>
				<li><a id="compte" href="adherent.jsp"><span>Mon compte</span></a></li>
			<%
				if (adh instanceof Administrateur) {
			%>
			<li><span id="admin"> Administration </span>
				<ul> 
					<li class="first"> <a href="ajout_adherent.jsp">Ajouter un membre</a></li>
					<li><a href="ajout_oeuvre.jsp">Ajouter une oeuvre</a></li>
					<li><a href="GestionEmprunt?type=demande">Gérer les demandes d'emprunt</a>
					<li><a href="GestionEmprunt?type=attente">Gérer les emprunts en attente de réception</a>
					<li><a href="GestionEmprunt?type=retour">Gérer les retours</a>
				</ul>
			</li>
				<% } %>
			<% } %>
			<% if (cart != null && 
					!cart.isEmpty()) { %>
				<li><a id="panier" href="panier.jsp"><span>Panier</span></a></li>

			<%} %>
		</ul>
            <script type="text/javascript">
                $('#menu').dropotron();
            </script>
        </div>