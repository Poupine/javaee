<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,entities.biblion7EE.*"%>
<div id="sidebar-bg">
	<div id="sidebar">
		<ul>
			<%@ include file="recherche_rapide.html" %>
			<%@ include file="horaires.html" %>
			<%ArrayList<Oeuvre> news = (ArrayList<Oeuvre>) request.getAttribute("news"); %>
			<li>
				<h2>Nouveautés</h2>
				<ul>
					<%if(news!=null){ %>
						<% for(Oeuvre o : news){%>
							<li><a href="GestionAffichageOeuvre?auteur=<%=o.getAuteur()%>&titre=<%=o.getTitre()%>"><%=o.getTitre() %></a></li>
						<% }%>
					<%} %>
				</ul>
			</li>
		</ul>
	</div>
</div>
<!-- end #sidebar -->