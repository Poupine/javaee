<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,entities.biblion7EE.*"%>
    
    <%Oeuvre oeuvre= (Oeuvre) request.getAttribute("affiche");  %>
    <%String title = oeuvre.getTitre()+" de "+oeuvre.getAuteur();%>
    <%
    String body="";
    if (oeuvre instanceof Livre) {
	    body = "livre";
	} else if (oeuvre instanceof DVD) {
		body = "dvd";
	} else {
		body = "cd";
	}%>
   		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp" %> 
		<% if (request.getAttribute("news") == null)  {%>
			<jsp:include page="GestionSidebar?from=affiche_oeuvre.jsp" flush="false"/>
		<%} %>
		<div id="page">

			<%@ include file="include/sidebar.jsp" %>

            <div id="content">
                <div class="contentbg">
                    <table>
                        <colgroup span="1" width="2000" align="left" />
                        <colgroup span="1" width="5000" align="right" />
                        <tr>
                            <th>
                                <h1><%=oeuvre.getTitre()%></h1>
                                <h3><%=oeuvre.getAuteur()%></h3>
                           	<%if (oeuvre instanceof DVD) { %>
					      		<h3> Producteur : <% out.print(((DVD)oeuvre).getProducteur()); %> </h3>
					      	<% } else if (oeuvre instanceof Livre){ %>
					      		<h3> Editeur : <% out.print(((Livre)oeuvre).getEditeur()); %> </h3>
					      	<% } else if (oeuvre instanceof CD){ %>
					      		<h3> Compositeur : <% out.print(((CD)oeuvre).getCompositeur()); %> </h3>
					      	<%} %>
                                <h5><%=oeuvre.getGenre()%></h5>
                                Paru en <%=oeuvre.getAnneeParution()%>.
                            </th>
                            <% if(oeuvre.getImage()!=null){%>
                            <th>
                                <img src="Image?ImageName=<%=oeuvre.getImage()%>" height="200px"/>
                            </th>
                            <%}%>
                        </tr>
                        <tr/>
                        <tr>
                            <% if(oeuvre.getImage()!=null){%>                        	
                            <th>
                            
                            </th>
                            <%}%>                            
                            <th>
							  Il est présent en <%=oeuvre.getNb_exemplaires()%> exemplaire.<br>
							  <%if (oeuvre.getNb_exemplaires() != 0 && currentSession.getAttribute("session") != null) { %>
									<%currentSession.setAttribute("oeuvre", oeuvre); %>
									<a href="GestionPanier?action=oeuvre">Ajouter au panier</a>
							  <%} %>							
                            </th>
                        </tr>
                    </table>
                         <h2>Résumé</h2>
                    <br/>
                    	<%=oeuvre.getDescription()%>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            <!-- end #content -->
            

	</div>
	<!-- end #page -->

<%@ include file="include/footer.html" %>
		