

<%@page import="java.util.ArrayList"%>
<%@page import="entities.biblion7EE.Emprunt"%>
<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<% String title = "Mon compte";
   String body  = "compte"; %>
<%@ include file="include/header.jsp" %>


<%
   Adherent current = (Adherent)currentSession.getAttribute("session");
   if (current == null) {
   response.sendRedirect("");
   } else {%>

<%@ include file="include/menu.jsp" %>
<div id="page">
  
  
  
  <div id="content">
    <div class="contentbg">
      <div class="post">
        <div id="Div1">
          <h1>Informations personnelles</h1>
	  <p></p>

	  <table id="hor-zebra">
	    <tr class="odd">
	      <td>nom - prenom</td>
	      <td><%= current.getNom() + " " + current.getPrenom() %></td>
	    </tr>
	    <tr>
	      <td>date de naissance</td>
	      <td><%=current.getDateNaissance() %></td>
	    </tr>
	    <tr class="odd">
	      <td>identifiant</td>
	      <td><%= current.getIdentifiant() %></td>
	    </tr>
	    <tr>
	      <td>adresse</td>
	      <td><%= current.getAdresse() %></td>
	    </tr>
	    <tr class="odd">
	      <td>numéro de teléphone</td>
	      <td><%= current.getTelephone() %></td>
	    </tr>
	    <tr>
	      <td>email</td>
	      <td><%= current.getEmail() %></td>
	    </tr>
	    <tr class="odd">
	      <td>date d'adhésion</td>
	      <td><%= current.getDateAdhesion() %></td>
	    </tr>
	  </table>

	  <p/><p/>

	  <table id="newspaper-a">
	    <caption> Emprunts </caption>
	    <thread>
	      <tr>
	      	<th scope="col"> Nom de l'oeuvre </th>
	      	<th scope="col"> Date d'emprunt  </th>
	      	<th scope="col"> Date de retour  </th>
	      </tr>
	    </thread>
	    <%
	    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Emprunt emprunt = (Emprunt)currentSession.getAttribute("emprunt");
		Object [] listeEmprunts = emprunt == null ? new Object[0] : emprunt.getListe().toArray();
	    for (Object o : listeEmprunts) {%>
		    <tbody>
		      <tr>
		      	<td> <%=((Oeuvre)o).getAuteur() %> </td>
		      	<td> <%=dateFormat.format(new Date(current.getEmprunt().getDateValidation())) %>  </td>
		      	<td> <%=dateFormat.format(new Date(current.getEmprunt().getDateValidation() + 604800000)) %>  </td>
		      </tr>
		    </tbody>
	    <%} %>
	  </table>



<!-- 	  <table id="newspaper-a">
	    <caption> Réservation </caption>
	    <thread>
	      <tr>
	      	<th scope="col"> Nom de l'oeuvre </th>
	      	<th scope="col"> Date de demande  </th>
	      </tr>
	    </thread>
	    <tbody>
	      <tr>
	      	<td> Titre </td>
	      	<td> Date  </td>
	      </tr>
	      <tr>
	      	<td> Titre </td>
	      	<td> Date  </td>
	      </tr>
	    </tbody>
	  </table>-->

        </div>
      </div>
      <div style="clear: both;">&nbsp;</div>
    </div>
  </div>
  <!-- end #content -->
  
  <div id="sidebar-bg">
    <div id="sidebar">
      <ul>
	<li>
	  <%@ include file="include/recherche_rapide.html" %> 
	</li>
	<li>
	  <h2>Informations personnelles</h2>
	  <p>
	    Voici vos informations personnelles.
	    Sur cette page, vous pouvez voir vos emprunts actuels et en attente.
	    Nous vous rappelons que tout retard sera sanctionné.
	  </p>
	</li>
	<li>
	  <h2>Modifications</h2>
	  <p>
	    <a href="ModificationInfos?champ=adresse">Modifier adresse postale</a>
	    <!--                          		<br><a href="ModificationInfos?champ=email">Modifier adresse email</a>-->
	    <br><a href="ModificationInfos?champ=password">Modifier mot de passe</a>
	  </p>
	</li>
      </ul>
    </div>
    <!-- end #sidebar -->
    
    <div style="clear: both;">&nbsp;</div>
  </div>
</div>
<%@ include file="include/footer.html" %>
<%} %>
