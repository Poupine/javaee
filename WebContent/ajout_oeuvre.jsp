<%@page import="entities.biblion7EE.DVD"%>
<%@page import="entities.biblion7EE.Livre"%>
<%@page import="entities.biblion7EE.Administrateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<% String title = "Ajout d'une oeuvre";
		   String body  = "admin"; %>
		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp"%>
		
	<script type="text/javascript" src="javascript/tiny_mce/tiny_mce.js"></script>
	
	<script type="text/javascript">
		tinyMCE.init({
		        mode : "textareas",
		        theme : "simple"
		});
	</script>
		
	<script language="JavaScript">
		function DispForm(choix)
        {
			document.getElementById('Livre').style.display = 'none';
			document.getElementById('CD').style.display = 'none';
			document.getElementById('DVD').style.display = 'none';
			
			if (choix == "livre") {
				document.getElementById('Livre').style.display = 'block';
			}
			else if (choix == "cd") {
				document.getElementById('CD').style.display = 'block';
			}
			
			else if (choix == "dvd") {
				document.getElementById('DVD').style.display = 'block';
			}
        }
	</script>
		
	<%
	String error = request.getParameter("error");
	Adherent current = (currentSession != null ? (Adherent)currentSession.getAttribute("session") : null);
	if (current == null || ! (current instanceof Administrateur)) {
		response.sendRedirect("");
	} else {%>
	<div id="page">
		<div id="content">
			<div class="contentbg">
				<div class="post">
					<div id="Div1">
						<h1>Administration oeuvres</h1>
						<form class="formulaire" method="post" action="GestionOeuvre" enctype="multipart/form-data">
							<input type="hidden" name="operation" value="ajouter_adherent">
						    <p> Type </p>
                            <select id="categorie" name="categorie" size="1" onChange="javascript: DispForm(document.getElementById('categorie').value);">
                                <option value="livre">Livre</option>
                                <option value="dvd">DVD</option>
                                <option value="cd">CD</option>                                
                            </select>
							<label for="titre">Titre</label>
								<input type="text" name="titre" required placeholder="Titre" />
							
							
							<div id='Livre' style="display: block;">
								<label for="auteur">Auteur</label>
								<input type="text" name="auteur"  placeholder="Auteur" />
								<label for="editeur">Éditeur</label>
								<input type="text" name="editeur"  placeholder="Éditeur" />
								<label for="genreLivre">Genre</label>
								<select  name="genreLivre">
									<option>Classique</option>
									<option>Fantasy</option>
									<option>Policier</option>
								</select>
							</div>
							
							<div id='CD' style="display: none;">
								<label for="chanteur">Chanteur</label>
								<input type="text" name="chanteur"  placeholder="Chanteur" />
								<label for="compositeur">Compositeur</label>
								<input type="text" name="compositeur"  placeholder="Compositeur" />
								<label for="genre">Genre</label>
								<select  name="genreCD">
									<option>Classique</option>
									<option>JPop</option>
									<option>Rock</option>
								</select>
							</div>	
							<div id='DVD' style="display: none;">
								<label for="realisateur">Réalisateur</label>
								<input type="text" name="realisateur"  placeholder="Réalisateur" />
								<label for="producteur">Producteur</label>
								<input type="text" name="producteur"  placeholder="Producteur" />
								<label for="genreDVD">Genre</label>
								<select  name="genreDVD">
									<option>Action</option>
									<option>Western</option>
									<option>Anticipation</option>
								</select>
							</div>
							<!--  
							    <label for="editeur">Editeur/Producteur/Compositeur</label>
								<input type="text" name="editeur" required placeholder="Editeur/Producteur/Compositeur" />
							 -->
							<label for="annee">Année de parution</label>
								<input type="text" name="annee" required placeholder="1992" pattern="[1-9][0-9][0-9][0-9]"/>
							<label for="nbex">Nombre d'exemplaires</label>
								<input type="text" name="nbex" value="1" pattern="[1-9]*"/>
							
							<label for="description">Description</label>
								<textarea name="description"  placeholder="Description"></textarea>
							<label for="image">Miniature</label>
								<input type="file" name="image" />
							<input type="submit" value="Ajouter" />
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- end #content -->
		<div id="sidebar-bg">
			<div id="sidebar">
				<ul>
					<li>
						<h2>Ajout Oeuvre</h2>
						<p>Ajoute une nouvelle oeuvre dans la base de données.</p>
					</li>
					<li>
						<h2>Autres Actions</h2>
						<ul>
							<li><a href="ajout_exemplaire.jsp">Ajout des exemplaires</a></li>
							<li><a href="ajout_adherent.jsp">Ajout d'un adhérent</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<%@ include file="include/footer.html" %>
	<!-- end #footer -->
	<% } %>