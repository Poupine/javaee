<%@page import="java.util.ArrayList"%>
<%@page import="entities.biblion7EE.Livre"%>
<%@page import="entities.biblion7EE.DVD"%>
<%@page import="entities.biblion7EE.CD"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.HashSet"%>
<%@page import="entities.biblion7EE.Oeuvre"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<% String title = "Mon panier";
		   String body  = "panier"; %>
		<%@ include file="include/header.jsp" %>
		
	<%
	Adherent current = (Adherent)currentSession.getAttribute("session");
	ArrayList<Oeuvre> panier = new ArrayList<Oeuvre>();
	if (current == null) {
		response.sendRedirect("");
	} else {%>
	
	<%@ include file="include/menu.jsp" %>
	
	<SCRIPT LANGUAGE="JavaScript">
		<!-- Begin
		function checkAll(field)
		{
			if (typeof field.length == "undefined") {
				field.checked = true;
			}
			else {
				for (i = 0; i < field.length; i++)
					field[i].checked = true ;
			}
		}
		
		function uncheckAll(field)
		{
			if (typeof field.length  == "undefined") {
				field.checked = false;
			}
			else {
				for (i = 0; i < field.length; i++)
					field[i].checked = false ;
			}
		}
		//  End -->
	</script>
	
			<div id="page">

            <div id="content">
                <div class="contentbg">
                    <% 
                    if (cart == null) {
                    	out.print(" Nous sommes désolé, notre serveur a rencontré un problème !");
                    }
                    else if (cart.size()==0) {
                    	response.sendRedirect("");
                    }
                    else {%>
					<form method="post" name="panier" action="GestionPanier">
						<table id="newspaper-a">
						<caption> <strong> Panier </strong> </caption>
	    				<thread>
						      <tr>
						      	<th scope="col"> Titre de l'oeuvre </th>
						      	<th scope="col"> Auteur/Réalisateur/Chanteur  </th>
						      	<th scope="col"> Editeur/Producteur/Compositeur </th>
						      	<th scope="col"> Année de parution </th>
						      	<th scope="col"> Genre </th>
						      	<th scope="col"> Supprimer/Valider </th>
							  </tr>
						 </thread>
						<% 
						int opt_id = 0;
						for(Iterator i = cart.iterator(); i.hasNext();) {
							Oeuvre oeuvre = (Oeuvre)i.next();
						%>
							<tbody>
								<tr>
									<td>
										<%=oeuvre.getTitre() %>
									</td>
									<td>
										<%=oeuvre.getAuteur() %>
									</td>
									<% if (oeuvre instanceof DVD) {%>
									<td>
										<%=((DVD)oeuvre).getProducteur() %>
									</td>
									<%} else if ( oeuvre instanceof Livre) { %>
									<td>
										<%=((Livre)oeuvre).getEditeur() %>
									</td>
									<%} else if ( oeuvre instanceof CD) { %>
									<td>
										<%=((CD)oeuvre).getCompositeur() %>
									</td>
									<%} %>
									<td>
										<%= oeuvre.getAnneeParution() %>
									</td>
									<% if (oeuvre instanceof DVD) {%>
									<td>
										<%=((DVD)oeuvre).getGenre() %>
									</td>
									<%} else if ( oeuvre instanceof Livre) { %>
									<td>
										<%=((Livre)oeuvre).getGenre() %>
									</td>
									<%} else if ( oeuvre instanceof CD) { %>
									<td>
										<%=((CD)oeuvre).getGenre() %>
									</td>
									<%} %>
									<td>
										<input type="checkbox" name="option" value="<%=opt_id++%>">
										<% panier.add(oeuvre);%>
									</td>
								</tr>
							</tbody>
						<% }
						currentSession.setAttribute("panier", panier);
                    }
					%>
						<tbody>
							<tr>
								<td>
								    <input class="button" type="button" name="CheckAll" value="Check All"
									onClick="checkAll(document.panier.option)">
								</td>
								
								<td>
									<input class="button" type="button" name="UnCheckAll" value="Uncheck All"
									onClick="uncheckAll(document.panier.option)">
								</td>
								<td></td>
								<td></td>
								<td>
									<input class="button" type="submit" name="action" value="Emprunter" />
								</td>
								<td>
									<input class="button" type="submit" name="action" value="Supprimer" />
								</td>
							</tr>
					</table>
				</form>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            
            <!-- end #content -->
            <div id="sidebar-bg">
                <div id="sidebar">
                    <ul>
				      <li>
						<%@ include file="include/recherche_rapide.html" %> 
		                    </li>
		                        <li>
		                            <h2>Panier</h2>
		                            <p>
										Dans cette section, vous pouvez revoir les différentes oeuvres présentes
										dans votre panier afin de valider ou non leur emprunt.
										Nous vous rappelons que vous ne pouvez emprunter plus de 10 oeuvres, toute
										catégorie confondue, à la fois.
		                            </p>
		                        </li>
                    </ul>
            </div>
            <!-- end #sidebar -->


	</div>
	<!-- end #page -->
	<% }%>