<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

		<% String title = "Modifications";
		   String body  = "compte"; %>
		<%@ include file="include/header.jsp" %>
		
		
	<% if (currentSession == null) {
		response.sendRedirect("");
	}
	String champ = request.getParameter("champ");
	String error = request.getParameter("error");
	%>
	
	<%@ include file="include/menu.jsp" %>
	

        <div id="page">
            <div id="content">
                <div class="contentbg">
                    <div class="post">
                        <div id="Div1">
                            <h1>Modification de l'adresse</h1>
							<p></p>
							<p>
							<form class="formulaire" method="post" action="ModificationInfos">
							<% if (champ != null && champ.equals("adresse")) { %>
									<label for="adresse">Adresse</label>
								<textarea name="adresse" required placeholder="Adresse"></textarea>
							<% } else { %>
								<label for="actual">Nouveau mot de passe</label>
								<input type="password" name="actual" required placeholder="Actuel" /> 
								<label for="password">Nouveau mot de passe</label>
								<input type="password" name="password" required placeholder="Nouveau" /> 
								<label for="confirm">Confirmer mot de passe</label>
								<input type="password" name="confirm" required placeholder="Confirmer" />
							<% } 
							if (error != null) { %>
								<p> Les mots de passe doivent correspondre</p>
							<% } %>
								<input type="submit" value="Valider" />
								</form>
							</p>
                        </div>
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            <!-- end #content -->
            <div id="sidebar-bg">
                <div id="sidebar">
                    <ul>
		      <li>
		<% if (champ != null && champ.equals("adresse")) { %>
		      	<h2> Modification de l'adresse</h2>
		      	<p> 
		      	Veuillez indiquer votre nouvelle adresse dans le champ de texte
		      	prévu à cet effet. Si tout c'est bien passé, vous serez redirigé
		      	vers la page "Mon compte".
		      	</p>
		 <% } else { %>
		      	<h2> Modification du mdp</h2>
		      	<p> 
		      	Veuillez indiquer votre nouveau mot de passe, en prenant soin de 
		      	le confirmer. Pour ce faire, vous aurez également besoin de 
		      	votre mot de passe actuel. Si tout c'est bien passé, vous serez redirigé
		      	vers la page "Mon compte".
		      	</p>
		 <% } %>		 
              </li>
            </div>
            <!-- end #sidebar -->
            <div style="clear: both;">&nbsp;</div>
        </div>
    </div>
    <%@ include file="include/footer.html" %>
	