<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

		<% String title = "";
		   String body  = "index"; %>
		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp" %>
		<% if (request.getAttribute("news") == null)  {%>
			<jsp:include page="GestionSidebar?from=index.jsp" flush="false"/>
		<%} %>
		<%@ include file="include/sidebar.jsp" %>
		<div id="page">
			<div id="content">
				<div class="contentbg">
					<div class="post">
						<div id="Div1">

					<h2>Mediathèque en ligne</h2>
					<br />
					<br />
					<br />
					<div id="slider">
						<div class="viewer">
							<div class="reel">
								<div id="gallery">
						<%if(news!=null){ %>
						  <% for(Oeuvre o : news){%>
						  	<%if(o.getImage()!=null){ %>
									<div class="slide">
										<a href="GestionAffichageOeuvre?auteur=<%=o.getAuteur()%>&titre=<%=o.getTitre()%>"><img
											src="Image?ImageName=<%= o.getImage() %>" alt=""height="300"/><!--  width="590" height="300" alt="" />--></a>
									</div>
							<% }%>
						  <% }%>
					    <%}else{ %>
								<div class="slide">
										<a href="images/pics02.jpg"><img
											src="images/pics02.jpg" width="590" height="300" alt="" /></a>
									</div>
									<div class="slide">
										<a href="images/pics03.jpg"><img
											src="images/pics03.jpg" width="590" height="300" alt="" /></a>
									</div>
							
							<% }%>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
					<script type="text/javascript">
				$('#slider').slidertron({
					viewerSelector: '.viewer',
					reelSelector: '.viewer .reel',
					slidesSelector: '.viewer .reel .slide',
					advanceDelay: 3000,
					speed: 'slow'
				});
			</script>
				</div>
			</div>
			<!-- end #content -->
			
			
			<div style="clear: both;">&nbsp;</div>

		</div>
		<!-- end #page -->
		
		<%@ include file="include/footer.html" %>
