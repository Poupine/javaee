<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

		<% String title = "exemplaire";
		   String body  = "examplaire"; %>
		<%@ include file="include/header.jsp" %>
		<%@ include file="include/menu.jsp" %>
		
        <div id="page">
            <div id="content">
                <div class="contentbg">
                    <div class="post">
                        <div id="Div1">
                            
                            

                            <h1>Ajouter un exemplaire</h1>
                            <form class="formulaire" action="formdemo.php" method="post">
                            <label for="text">ISBN</label>
                            <input type="text" name="isbn" placeholder="isbn" />
                            <label for="text">Numéro d'emplaire</label>
                            <input type="text" name="numero" placeholder="Numero" />
			    <!-- remplie automatiquement -->
                            <label for="text">Identifiant</label>
                            <input type="text" name="Identifiant" placeholder="Identifiant" />
			    <!-- remplie automatiquement -->
                            <label for="text">Cote</label>
                            <input type="text" name="cote" placeholder="Cote" />
			    <p> Type </p>
                            <select name="categorie" size="1">
                                <option>Livre</option>
                                <option>CD</option>
                                <option>DVD</option>
                            </select>
			    <p> Disponibilité </p>
                            <select name="disonibilite" size="1">
                                <option>Disponible</option>
                                <option>En reparation</option>
                                <option>Emprunté</option>
                            </select>
                            <input type="submit" value="Ajouter" />
                            </form>
                        </div>
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
            <!-- end #content -->
            <div id="sidebar-bg">
                <div id="sidebar">
                    <ul>
                        <li>
                            <h2>Ajout d'un exemplaire</h2>
                            <p>
			      Ajout d'un nouvel exemplaire d'une oeuvre.
                            </p>
                        </li>
                        <li>
                            <h2>Autres Actions</h2>
                            <ul>
                                <li><a href="ajout_adherent.jsp">Ajout d'un adhérent</a></li>
                                <li><a href="oeuvre.jsp">Ajout d'une oeuvre</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- end #sidebar -->
            <div style="clear: both;">&nbsp;</div>
        </div>
        <!-- end #page -->
    </div>
    
<%@ include file="include/footer.html" %>
