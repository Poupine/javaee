<%@page import="ejb.biblion7EE.BDDAdherentItf"%>
<%@page import="entities.biblion7EE.Administrateur"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="biblion7EE.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

    <% String title = "Ajout d'un nouvel adhérent";
	   String body  = "admin"; %>
	<%@ include file="include/header.jsp"%>
	<%@ include file="include/menu.jsp"%>
	<!-- Contient le calendrier utilisé pour spécifier la date de naissance -->
	<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass"
		style="display: none;">
		<tr>
			<td id="ds_calclass"></td>
		</tr>
	</table>
	<div id="page">
		<div id="content">
			<div class="contentbg">
				<div class="post">
					<div id="Div1">
						<h1>Administration adhérent</h1>

						<form class="formulaire" method="post" action="Gestion">
							<input type="hidden" name="operation" value="ajouter_adherent">
								<% String error = request.getParameter("error");
							   AdminLevel level = null;
							   Adherent adh = (currentSession != null ? (Adherent)currentSession.getAttribute("session") : null);
							   if (adh != null && adh instanceof Administrateur) {
									level = (AdminLevel)((Administrateur)adh).getNiveau();
							   } else {
								   response.sendRedirect("");
							   }
							   
                               if(error == null) {%> 
                               <label for="name">Identifiant</label>
									<input type="text" name="identifiant" required placeholder="Identifiant" /> 
								<label for="name">Nom</label> 
									<input type="text" name="nom" required placeholder="Nom" /> 
								<label for="name">Prénom</label> 
									<input type="text" name="prenom" required placeholder="Prénom" /> <label for="adresse">Adresse</label>
								<textarea name="adresse" required placeholder="Adresse"></textarea>
								<label for="telephone">Téléphone</label>
									<input type="telephone" name="telephone" required placeholder="0123456789" pattern="^[0-9]{10}$" /> 
								<label for="email">Email</label> 
									<input type="email" name="email" required placeholder="email@example.com" /> 
								<label for="date_naissance">Date de naissance</label> 
									<input type="date" name="naissance" required readonly="true" placeholder="31/12/42" onclick="ds_sh(this);" /> 
									<% if (level == AdminLevel.root) { %>
									<select name="admin" size="1">
										<option value="none" selected> None
										<option value="bibliothecaire"> Bibliothécaire
										<option value="root"> Root
									</select>
									<% } %>
								<%} else {%> 
								<label for="name">Identifiant</label> 
								<% if (error.equals("identifiant")) { %>
									<input type="text" name="identifiant" required placeholder="Identifiant <%=request.getParameter("identifiant") %> déjà utilisé" />
								<% } else { %> 
									<input type="text" name="identifiant" value="<%=request.getParameter("identifiant") %>" required placeholder="Identifiant" /> 
								<% } %>
								<label for="name">Nom</label>
									<input type="text" name="nom" value="<%=request.getParameter("nom") %>" required placeholder="Nom" />
								<label for="name">Prénom</label>
								<input type="text" name="prenom" value="<%=request.getParameter("prenom") %>" required placeholder="Prénom" />
								<label for="adresse">Adresse</label> 
									<textarea name="adresse" placeholder="Adresse"><%=request.getParameter("adresse") %></textarea>
								<label for="telephone">Téléphone</label> 
									<input type="telephone" name="telephone" required placeholder="0123456789" value="<%=request.getParameter("telephone") %>" pattern="^[0-9]{10}$" />
								<label for="email">Email</label> 
								<% if (error.equals("mail")) { %>
									<input type="email" name="email" required placeholder="Email déjà présent dans la BDD" /> 
								<%} else {%> 
									<input type="email" name="email" required placeholder="email@example.com" value="<%=request.getParameter("email") %>" /> 
								<%} %> 
								<label for="date_naissance">Date de naissance</label>
									<input type="date" name="naissance" readonly="true" required placeholder="31/12/1942" onclick="ds_sh(this);" value="<%=request.getParameter("naissance")%>" /> 
									<% if (level == AdminLevel.root) { %>
									<select name="admin" size="1">
										<option value="none" selected> None
										<option value="bibliothecaire"> Bibliothécaire
										<option value="root"> Root
									</select>
									<% } %>
								<%}%> 
								<input type="submit" value="Valider" />
						</form>
					</div>
				</div>
				<div style="clear: both;">&nbsp;</div>
			</div>
		</div>
		<!-- end #content -->
		<div id="sidebar-bg">
			<div id="sidebar">
				<ul>
					<li>
						<h2>Ajout Adherent</h2>
						<p>Ajoute un nouvel adhérent dans la base de donnée.</p>
					</li>
					<li>
						<h2>Autres Actions</h2>
						<ul>
							<li><a href="ajout_exemplaire.jsp">Ajoute des exemplaires</a></li>
							<li><a href="ajout_oeuvre.jsp">Ajout d'une oeuvre</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<%@ include file="include/footer.html" %>
	<!-- end #footer -->
</body>
</html>
