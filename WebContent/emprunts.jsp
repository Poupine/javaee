<%@page import="java.util.Date"%>
<%@page import="entities.biblion7EE.Emprunt"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

    <% String title = "Gestion des emprunts";
	   String body  = "admin"; %>
	<%@ include file="include/header.jsp"%>
<%
   Adherent current = (Adherent)currentSession.getAttribute("session");
   if (current == null || !(current instanceof Administrateur)) {
		response.sendRedirect("");
   } else {%>
	<%@ include file="include/menu.jsp"%>
	
	<SCRIPT LANGUAGE="JavaScript">
		<!-- Begin
		function checkAll(field)
		{
			if (typeof field.length == "undefined") {
				field.checked = true;
			}
			else {
				for (i = 0; i < field.length; i++)
					field[i].checked = true ;
			}
		}
		
		function uncheckAll(field)
		{
			if (typeof field.length  == "undefined") {
				field.checked = false;
			}
			else {
				for (i = 0; i < field.length; i++)
					field[i].checked = false ;
			}
		}
		//  End -->
	</script>
	
	<%
		ArrayList<Emprunt> listeEmprunts = (ArrayList<Emprunt>)request.getAttribute("emprunts");
	%>
<div id="page">
  <div id="content">
    <div class="contentbg">
      <div class="post">
        <div id="Div1">
        <h1> Gestion des emprunts </h1>
      <form method="post"  name="emprunts" action="GestionEmprunt">
	  <table id="newspaper-a">
	  <% if (request.getParameter("type").equals("attente")) {%>
		    <caption> Liste des emprunts en attente </caption>
	  <% } else if (request.getParameter("type").equals("demande")) { %>
	  		<caption> Liste des demandes d'emprunt </caption>
	  <%} else {%>
	  		<caption> Rendu des emprunts </caption>
	  <%} %>
	    <thread>
	      <tr>
	      	<th scope="col"> Identifiant </th>
	      	<th scope="col"> Nom et prénom </th>
	      	<th scope="col"> Date de demande </th>
	      	<th scope="col"> Liste des oeuvres </th>
	      	<th scope="col">  </th>
	      </tr>
	    </thread>
	    <% 
	    int opt_id = 0;
	    ArrayList<Emprunt> valide = new ArrayList<Emprunt>();
	    for (Emprunt e : listeEmprunts) {%>
		    <tbody>
		      <tr>
		      	<td> <%=e.getAdherent().getIdentifiant() %> </td>
		      	<td> <%=e.getAdherent().getPrenom() + " " + e.getAdherent().getNom() %>  </td>
		      	<td> <%=new Date(e.getDateDemande()) %>  </td>
		      	<td> 
		      		<%for (Oeuvre o : e.getListe()) { 
		      			out.println(o.getTitre());%><br>
		      		<%}%>
		      	</td>
				<td>
					<input type="checkbox" name="option" value="<%=opt_id++%>">
					<% valide.add(e);%>
				</td>
		      </tr>
		    </tbody>
		<%} 
		currentSession.setAttribute("valide", valide);
		%>
		<%if (!listeEmprunts.isEmpty()) {%>
		<tbody>
			<tr>
				<td>
				    <input class="button" type="button" name="checkAll" value="Check All"
					onClick="checkAll(document.emprunts.option)">
				</td>
				
				<td>
					<input class="button" type="button" name="unCheckAll" value="Uncheck All"
					onClick="uncheckAll(document.emprunts.option)">
				</td>
				<td></td>
				<%if (request.getParameter("type").equals("attente")) { %>
					<td></td>
					<input type="hidden" name="type" value="attente"/>
				<%} %>
				<%if (request.getParameter("type").equals("retour")) { %>
					<input type="hidden" name="type" value="retour" />
				<%} %>
				<td>
					<input class="button" type="submit" name="action" value="Valider" />
				</td>
				<%if (request.getParameter("type").equals("demande")) { %>
					<td>
						<input type="hidden" name="type" value="demande"/>
						<input class="button" type="submit" name="action" value="En attente" />
					</td>
				<%} %>
			</tr>
		</tbody>
		<% } else {%>
		<tbody>
			<tr>
				<td>-</td>
				<td>-</td>
				<td>-</td>
				<td>-</td>
				<td>-</td>
			</tr>
		</tbody>
		<%} %>
	  </table>
	  </form>

        </div>
      </div>
      <div style="clear: both;">&nbsp;</div>
    </div>
  </div>
  <!-- end #content -->
  
  <div id="sidebar-bg">
    <div id="sidebar">
      <ul>
	<li>
	  <h2>Liste des emprunts</h2>
	  <p>
		Dans cette section, vous pouvez gérer les emprunts de vos utilisateurs.
		Ainsi, une fois les oeuvres prêtes, vous pouvez soit directement valider
		l'emprunt si l'utilisateur se trouve à la bibliothèque, soit les mettre 
		en attente. Cette dernière action enverra un e-mail à l'utilisateur afin
		de le prévenir que ses oeuvres sont prêtes à être retirées.
	  </p>
	</li>
      </ul>
    </div>
    <!-- end #sidebar -->
    
    <div style="clear: both;">&nbsp;</div>
  </div>
</div>
<%@ include file="include/footer.html" %>
<%} %>
	