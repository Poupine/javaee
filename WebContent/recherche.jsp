<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <% String title = "recherche";
	   String body  = "recherche"; %>
	<%@ include file="include/header.jsp"%>
	<%@ include file="include/menu.jsp"%>
	
	<script language="JavaScript">
		function DispForm(choix)
        {
			document.getElementById('Livre').style.display = 'none';
			document.getElementById('CD').style.display = 'none';
			document.getElementById('DVD').style.display = 'none';
			
			if (choix == "Livre") {
				document.getElementById('Livre').style.display = 'block';
			}
			else if (choix == "CD") {
				document.getElementById('CD').style.display = 'block';
			}
			
			else if (choix == "DVD") {
				document.getElementById('DVD').style.display = 'block';
			}
        }
	</script>
	
	<div id="page">
			
		<div id="content">
			<div class="contentbg">
				<div class="post">
					<div id="Div1">



						<h1>Recherche Avancée</h1>
						<form class="formulaire" action="Recherche" method="get">
						
							<input type="hidden" name="type" value="avance">
						
							<select id="categorie" name="categorie" size="1" onChange="javascript: DispForm(document.getElementById('categorie').value);">
								<option value=""></option>
								<option>Livre</option>
								<option>DVD</option>
								<option>CD</option>
							</select> 
							
							<label for="titre">Titre</label>
							<input type="text" name="titre" placeholder="Titre" /> 
							
							<label for="name">Auteur/Réalisateur/Chanteur</label> 
							<input type="text" name="auteur" placeholder="Auteur" /> 
							
							<label for="text">Date de parution</label> 
							<input type="text" name="parution" placeholder="1990" /> 
							
							<label for="text">Genre</label> 
							<input type="text" name="genre" placeholder="genre" /> 
							
							<div id='Livre' style="display: none;">
								<label for="Producteur">Éditeur</label>
								<input type="text" name="editeur" placeholder="Éditeur" />
							</div>
							<div id='CD' style="display: none;">
								<label for="Producteur">Compositeur</label>
								<input type="text" name="compositeur" placeholder="Compositeur" />
							</div>	
							<div id='DVD' style="display: none;">
								<label for="Producteur">Producteur</label>
								<input type="text" name="producteur" placeholder="Producteur" />
							</div>
							
							<input type="submit" value="Rechercher" />
						</form>
					</div>
				</div>
				<div style="clear: both;">&nbsp;</div>
			</div>
		</div>
		<!-- end #content -->
		<div id="sidebar-bg">
			<div id="sidebar">
				<ul>

					<li>
						<h2>Recherche Avancé</h2>
						<p>La recheche est un outil vous permettant de consulter notre
							base de données. Vous n'êtes pas obligé de remplir tous les
							champs. Plus il y aura de champs remplis plus precise sera la
							recherche</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<!-- end #page -->
<%@ include file="include/footer.html" %>