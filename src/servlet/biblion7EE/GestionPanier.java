package servlet.biblion7EE;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.biblion7EE.BDDAdherentItf;
import ejb.biblion7EE.BDDEmpruntItf;
import ejb.biblion7EE.BDDOeuvreItf;
import entities.biblion7EE.Adherent;
import entities.biblion7EE.Oeuvre;

import biblion7EE.Parser;

/**
 * Servlet implementation class GestionPanier
 */
public class GestionPanier extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	BDDEmpruntItf psql;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionPanier() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String referer = request.getHeader("referer");
		String action = request.getParameter("action");
		HashSet<Oeuvre> cart = (HashSet<Oeuvre>)request.getSession().getAttribute("cart");
		
		if (Parser.parseReferer(referer).equals("") && !referer.contains(Parser.localAddress)) {
			response.sendRedirect("");
		} else {
			if (action.equals("Ajouter au panier")) {
				String[] checkboxes = request.getParameterValues("option");
				for (String s : checkboxes) {
					cart.add((Oeuvre)((ArrayList<Oeuvre>)request.getSession().getAttribute("addToCart")).get(Integer.parseInt(s)));
				}
				request.getSession().setAttribute("cart", cart);
			} else if (action.equals("Supprimer")) {
				ArrayList<Oeuvre> panier = (ArrayList<Oeuvre>)request.getSession().getAttribute("panier");
				String[] checkboxes = request.getParameterValues("option");
				for (String s : checkboxes) {
					cart.remove(panier.get(Integer.parseInt(s)));
				}
			} else if (action.equals("Emprunter")) {
				Adherent adh = (Adherent)request.getSession().getAttribute("session");
				psql.demanderEmprunt(adh, cart);
				cart.clear();
			} else if (action.equals("oeuvre")) {
				cart.add((Oeuvre) request.getSession().getAttribute("oeuvre"));
			}
		}
		if (!action.equals("oeuvre")) {
			response.sendRedirect("panier.jsp");
		} else {
			Oeuvre oeuvre = (Oeuvre)request.getSession().getAttribute("oeuvre");
			response.sendRedirect("GestionAffichageOeuvre?auteur=" + oeuvre.getAuteur() + "&titre=" + oeuvre.getTitre());
		}
	}

}
