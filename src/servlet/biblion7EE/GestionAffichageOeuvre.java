package servlet.biblion7EE;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblion7EE.Parser;

import ejb.biblion7EE.BDDOeuvre;
import ejb.biblion7EE.BDDOeuvreItf;
import entities.biblion7EE.CD;
import entities.biblion7EE.DVD;
import entities.biblion7EE.Livre;
import entities.biblion7EE.Oeuvre;
/**
 * Servlet implementation class GestionAffichageOeuvre
 */
public class GestionAffichageOeuvre extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	@EJB
	BDDOeuvreItf psql;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionAffichageOeuvre() {
        super();
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String auteur=request.getParameter("auteur");
		String titre=request.getParameter("titre");
		if(Parser.parseReferer(auteur).equals("") && Parser.parseReferer(titre).equals("")){
			List<Oeuvre> oeuvre = psql.recherche_avancee("",titre,auteur,"","");
			if(oeuvre != null && oeuvre.size() > 0){
				request.setAttribute("affiche",oeuvre.get(0));
				request.getRequestDispatcher("affiche_oeuvre.jsp").forward(request,response);
			}else{
				request.getRequestDispatcher("404.jsp").forward(request,response);
			}
		}else{
			response.sendRedirect("");
		}
	}
}



