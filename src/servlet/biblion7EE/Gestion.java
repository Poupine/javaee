package servlet.biblion7EE;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblion7EE.AdminLevel;
import biblion7EE.Md5;
import biblion7EE.Parser;
import biblion7EE.StatusAdherent;

import ejb.biblion7EE.BDDAdherentItf;
import ejb.biblion7EE.EmailSenderItf;
import entities.biblion7EE.Adherent;
import entities.biblion7EE.Administrateur;

/**
 * Servlet implementation class Gestion
 */
public class Gestion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private BDDAdherentItf psql;

	private EmailSenderItf emailsender;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Gestion() {
		super();
		try {
			InitialContext ic = new InitialContext();
			psql = (BDDAdherentItf) ic.lookup("BDDAdherent/local");
			emailsender = (EmailSenderItf) ic.lookup("EmailSender/local");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getParameter("operation");
		String referer = request.getHeader("referer");

		if (Parser.parseReferer(referer).equals("")) {
			response.sendRedirect("");
		} else {

			String goTo = "";

			if(operation.equals("ajouter_adherent")) {
				String identifiant = request.getParameter("identifiant");
				String nom = request.getParameter("nom");
				String prenom = request.getParameter("prenom");
				String adresse = request.getParameter("adresse");
				String telephone = request.getParameter("telephone");
				String email = request.getParameter("email");
				String date_naissance = request.getParameter("naissance");
				String password = String.valueOf(Math.abs((nom + prenom + identifiant).hashCode()));
				String date_adhesion = DateFormat.getDateInstance().format(new Date());
				String level = request.getParameter("admin");
				Adherent adherent = null;
				if (level != null && !level.equals("none")) {
					AdminLevel l;
					if (level.equals("bibliothecaire")) {
						l = AdminLevel.bibliothecaire;
					} else {
						l = AdminLevel.root;
					}
					adherent = new Administrateur(identifiant, password, 
							nom, prenom,
							adresse, telephone, 
							email, date_naissance,
							date_adhesion, l);
				} else {
					adherent = new Adherent(identifiant, password, 
							nom, prenom,
							adresse, telephone, 
							email, date_naissance,
							date_adhesion);
				}
				StatusAdherent status = psql.ajouterAdherent(adherent);
				adherent.setPassword(password);
				goTo = "";
				switch (status) {
				case err_identifiant :
					goTo = "GestionErreur?error=identifiant&from=ajout_adherent.jsp";
					break;
				case err_mail:
					goTo = "GestionErreur?error=mail&from=ajout_adherent.jsp";
					break;
				case adherentOK :
				default :
					goTo = "ok";
					break;
				}

				if (!goTo.equals("ok")) {
					request.getRequestDispatcher(goTo).forward(request, response);
				} else {
					emailsender.sendMailConfirmation(adherent);
					response.sendRedirect("");
				}
			}
		}
	}
}
