package servlet.biblion7EE;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ejb.biblion7EE.BDDAdherentItf;
import ejb.biblion7EE.EmailSenderItf;
import entities.biblion7EE.Adherent;

/**
 * Servlet implementation class ModificationInfos
 */
public class ModificationInfos extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private BDDAdherentItf psql;
	
	private EmailSenderItf emailsender;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModificationInfos() {
        super();
        try {
        	InitialContext ic = new InitialContext();
        	psql = (BDDAdherentItf) ic.lookup("BDDAdherent/local");
        	emailsender = (EmailSenderItf) ic.lookup("EmailSender/local");
        } catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("referer");
		String champ = request.getParameter("champ");
		String actuel = request.getParameter("actual");
		String nouveau = request.getParameter("password");
		String confirm = request.getParameter("confirm");
		String adresse = request.getParameter("adresse");
		String pseudo = ((Adherent)request.getSession().getAttribute("session")).getIdentifiant();
		
		if (referer == null) {
			response.sendRedirect("");
		} else {
			if (champ != null) {
				request.getRequestDispatcher("modification.jsp").forward(request, response);
			} else {
				if (adresse != null) {
					psql.setAdresse(pseudo, adresse);
					((Adherent)request.getSession().getAttribute("session")).setAdresse(adresse);
					response.sendRedirect("adherent.jsp");
				} else {
					if (nouveau.equals(confirm) && psql.setPassword(pseudo, actuel, nouveau)) {
						response.sendRedirect("adherent.jsp");
					} else {
						request.getRequestDispatcher("GestionErreur?error=nomatch&from=modification.jsp").forward(request, response);
					}
				}
			}
		}
	}

}
