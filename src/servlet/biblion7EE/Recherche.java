package servlet.biblion7EE;

import java.io.IOException;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblion7EE.Parser;

import ejb.biblion7EE.BDDOeuvreItf;

/**
 * Servlet implementation class Recherche
 */
public class Recherche extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	private BDDOeuvreItf psql;
	
    public Recherche() {
        super();
		try {
			InitialContext ic = new InitialContext();
			psql = (BDDOeuvreItf) ic.lookup("BDDOeuvre/local");
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type = "avance";
		if (request.getParameter("type") != null)
			type = request.getParameter("type");
//		String referer = request.getHeader("referer");
//		if (Parser.parseReferer(referer).equals("") && !referer.contains(Parser.localAddress)) {
//			response.sendRedirect("");
//		} else {		
		String categorie = "";
		String titre     = "";
		String auteur    = "";
		String parution  = "";
		String genre     = "";
		if (type.equals("avance")) {
			if (request.getParameter("categorie") != null)
				categorie = request.getParameter("categorie").toLowerCase();
			if (request.getParameter("titre") != null)
				titre     = request.getParameter("titre");
			if (request.getParameter("auteur") != null)
				auteur    = request.getParameter("auteur");
			if (request.getParameter("parution") != null)
				parution  = request.getParameter("parution");
//				String editeur   = request.getParameter("editeur");
			if (request.getParameter("genre") != null)
				genre     = request.getParameter("genre");
//				System.out.println(categorie);
//				System.out.println(titre);
//			System.out.println("!!!!!!!!!!!!");
//			System.out.println(categorie);
//			System.out.println(type);
			List resultats = psql.recherche_avancee(categorie, titre, auteur, parution, genre);
			request.getServletContext().setAttribute("resultats", resultats);
			response.sendRedirect("resultats.jsp");	
		}
		// (type.equals("simple"))
		else  {
			// FIXME check qu'elle existe
			String recherche = request.getParameter("recherche");
			List resultats = psql.recherche_rapide(recherche);
			request.getServletContext().setAttribute("resultats", resultats);
			response.sendRedirect("resultats.jsp");	
		}
//		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
