package servlet.biblion7EE;

import java.io.IOException;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import biblion7EE.Parser;

import ejb.biblion7EE.BDDEmprunt;
import ejb.biblion7EE.BDDEmpruntItf;
import ejb.biblion7EE.EmailSenderItf;
import entities.biblion7EE.Emprunt;

/**
 * Servlet implementation class GestionEmprunt
 */
public class GestionEmprunt extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	BDDEmpruntItf psql;
	
	@EJB
	EmailSenderItf emailSender;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionEmprunt() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("referer");
		String type = request.getParameter("type");
		ArrayList<Emprunt> listeEmprunts = null;

		if (referer != null && Parser.parseReferer(referer).equals("") && !referer.contains(Parser.localAddress)) {
			response.sendRedirect("");
		} else {
			if (type.equals("demande")) {
				listeEmprunts = psql.listeDemande();
			} else if (type.equals("attente")){
				listeEmprunts = psql.listeAttente();
			} else {
				listeEmprunts = psql.listeEmpruntsValides();
			}
			request.setAttribute("emprunts", listeEmprunts);
			String action = request.getParameter("action");
			String[] checkboxes = request.getParameterValues("option");
			ArrayList<Emprunt> valide = (ArrayList<Emprunt>)request.getSession().getAttribute("valide");
			if (action != null) {
				if (action.equals("Valider")) {
					if (type.equals("retour")) {
						for (String s : checkboxes) {
							psql.retourEmprunt(valide.get(Integer.parseInt(s)));
						}
					} else {
						for (String s : checkboxes) {
							psql.validerEmprunt(valide.get(Integer.parseInt(s)));
						}
					}
				} else if (action.equals("En attente")) {
					for (String s : checkboxes) {
						Emprunt e = valide.get(Integer.parseInt(s));
						psql.attenteEmprunt(e);
						emailSender.sendMailEmprunt(e.getAdherent());
					}
				}
				if (type.equals("demande")) {
					request.setAttribute("emprunts", psql.listeDemande());
				} else if (type.equals("attente")) {
					request.setAttribute("emprunts", psql.listeAttente());
				} else {
					request.setAttribute("emprunts", psql.listeEmpruntsValides());
				}
			}
			request.getRequestDispatcher("emprunts.jsp").forward(request, response);
		}
	}

}
