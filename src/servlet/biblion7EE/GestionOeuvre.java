package servlet.biblion7EE;

import java.io.IOException;
import java.util.Hashtable;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javazoom.upload.*;
import javazoom.upload.parsing.*;

import java.util.Vector;
import biblion7EE.Parser;

import ejb.biblion7EE.BDDOeuvre;
import ejb.biblion7EE.BDDOeuvreItf;
import entities.biblion7EE.CD;
import entities.biblion7EE.DVD;
import entities.biblion7EE.Livre;
import entities.biblion7EE.Oeuvre;

/**
 * Servlet implementation class GestionOeuvre
 */
public class GestionOeuvre extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String upPath = "/home/postgresn7/upload";

	@EJB
	BDDOeuvreItf psql;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionOeuvre() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("referer");
		MultipartFormDataRequest mrequest = null;
		UploadBean upBean = null;

		if (Parser.parseReferer(referer).equals("") && !referer.contains(Parser.localAddress)) {
			response.sendRedirect("");
		} else {

			try
			{
				int uploadlimit = 1024*1024*1024; // 1GB                                                                                                                       
				Vector listeners = null; // No upload listeners                                                                                                                
				String parser = MultipartFormDataRequest.COSPARSER; // Cos parser                                                                                              
				String encoding = "iso-8859-1";
				mrequest = new MultipartFormDataRequest(request, listeners, uploadlimit, parser, encoding);
				upBean = new UploadBean();                                                                                
				upBean.setFolderstore(upPath);
			}
			catch (IOException ex)
			{
				ex.printStackTrace();
			}
			catch (UploadException ex)
			{
				ex.printStackTrace();
			}


			String categorie = mrequest.getParameter("categorie").toLowerCase();
			String titre = mrequest.getParameter("titre");
			String auteur = "";
			String editeur = "";

			String parution = mrequest.getParameter("annee");
			String nbex = mrequest.getParameter("nbex");
			String genre = "livre";
			if (categorie.equals("livre")) {
				auteur  = mrequest.getParameter("auteur");
				editeur = mrequest.getParameter("editeur");
				genre   = mrequest.getParameter("genreLivre").toLowerCase();
			}
			else if (categorie.equals("cd")) {
				auteur  = mrequest.getParameter("chanteur");
				editeur = mrequest.getParameter("compositeur");
				genre   = mrequest.getParameter("genreCD").toLowerCase();
			}
			else if (categorie.equals("dvd")) {
				auteur  = mrequest.getParameter("realisateur");
				editeur = mrequest.getParameter("producteur");
				genre   = mrequest.getParameter("genreDVD").toLowerCase();
			}

			String description = mrequest.getParameter("description");
			String image = "";
			Oeuvre oeuvre = null;
			Hashtable files = mrequest.getFiles();
			if ( files != null && !files.isEmpty() )
			{
				UploadFile file = (UploadFile) files.get("image");
				if (file != null) {
					try {
						image = (titre + auteur).hashCode() + "." + file.getContentType().split("/")[1];
						file.setFileName(image);
						upBean.store(mrequest);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}

			if (categorie.equals("dvd")) {
				oeuvre = new DVD(titre, auteur, genre, editeur, 
						Integer.parseInt(parution), Integer.parseInt(nbex),
						description, image);
			} else if (categorie.equals("livre")) {
				oeuvre = new Livre(titre, auteur, editeur, Integer.parseInt(parution), 
						Integer.parseInt(nbex), genre, description, image);
			} else if (categorie.equals("cd")) {
				oeuvre = new CD(titre,auteur,genre,editeur, 
						Integer.parseInt(parution),Integer.parseInt(nbex),
						description, image);
			}

			psql.ajouter(oeuvre);
		}
		response.sendRedirect("ajout_oeuvre.jsp");

	}

}
