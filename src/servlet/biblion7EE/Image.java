package servlet.biblion7EE;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Image
 */
public class Image extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String upPath = "/home/postgresn7/upload/";
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Image() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	{	
		try 
		{
 
			DataOutput output = new DataOutputStream( response.getOutputStream() );
			response.setContentType("image/jpeg");
 
			String nomImage = request.getParameter("ImageName");
 
			File file = null;
 
			FileInputStream in = null;			
 
			String filePath = upPath+ nomImage;
 
 
			file = new File(filePath);				
 
			/*
			 * Dans le cas ou l'image n'est pas présente dans le répertoire
			 * On affiche une image par defaut 'Image Introuvable'
			 */
			if(!file.exists())
			{
				String path = upPath + "noImage1.png";
				file = new File(path);
			}
 
			in = new FileInputStream(file);
 
			response.setContentLength((int)file.length());
 
			byte buffer[]=new byte[4096];
			int nbLecture;
 
			while( (nbLecture = in.read(buffer)) != -1 )
			{				
				output.write(buffer,0,nbLecture);					
			}	
 
			in.close();
 
		}
		catch (IOException e)
		{
		    e.printStackTrace();	
		}		
	}	

}
