package servlet.biblion7EE;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GestionErreur
 */
public class GestionErreur extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionErreur() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String errorType = request.getParameter("error");
		String referer = request.getHeader("referer");
		String goTo = "";
		if (referer != null) {
			if (errorType != null) {
				goTo = request.getParameter("from");
				if (!goTo.contains("jsp")) {
					goTo = "";
				}
				request.getRequestDispatcher(goTo).forward(request, response);
			} else {
				response.sendRedirect("");
			}
		} else {
			response.sendRedirect("");
		}
	}

}
