package servlet.biblion7EE;

import java.io.IOException;
import java.util.HashSet;


import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import biblion7EE.Md5;
import biblion7EE.Parser;

import ejb.biblion7EE.BDDAdherentItf;
import entities.biblion7EE.Adherent;
import entities.biblion7EE.Administrateur;
import entities.biblion7EE.Oeuvre;

/**
 * Servlet implementation class GestionConnexion
 */

public class GestionConnexion extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private BDDAdherentItf psql;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GestionConnexion() {
        super();
        try {
        	InitialContext ic = new InitialContext();
        	psql = (BDDAdherentItf) ic.lookup("BDDAdherent/local");
        } catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String referer = request.getHeader("referer");
		
		if (referer != null) {
			HttpSession session = request.getSession(false);
			if (session != null) {
				session.invalidate();
			}
		}
		response.sendRedirect("");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String goTo = "";
		String referer = request.getHeader("referer");
		String login, pwd = "";
		
		if (Parser.parseReferer(referer).equals("") && !referer.contains(Parser.localAddress)) {
			response.sendRedirect("");
		} else {
			goTo = Parser.parseReferer(referer);
			System.out.println(goTo);
			login = request.getParameter("pseudo");
			pwd = request.getParameter("pwd");
			
			Adherent adherent = psql.findAdherent(login);
			
			if (adherent != null && adherent.getPassword().equals(Md5.encode(pwd))) {
				HttpSession session = request.getSession(true);
				session.setAttribute("session", adherent);
				session.setAttribute("emprunt", adherent.getEmprunt());
				session.setAttribute("cart", new HashSet<Oeuvre>());
				session.setMaxInactiveInterval(-1);
			} else {
				goTo = "GestionErreur?error=logpass&from=" + Parser.parseReferer(referer);
			}
			if (!goTo.contains("error")) {
				response.sendRedirect(goTo);
			} else {
				request.getRequestDispatcher(goTo).forward(request, response);
			}
		}
	}

}
