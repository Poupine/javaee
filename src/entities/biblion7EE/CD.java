package entities.biblion7EE;

import javax.persistence.Id;
import javax.persistence.Entity;

import biblion7EE.IdOeuvre;

@Entity
public class CD extends Oeuvre{
	
	private String compositeur;
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre){
		this.titre = titre;
	}
	
	public String toString() {
			return String.format("titre : %s\nauteur : %s ...", titre, auteur);
	}
	
	public CD() {}
	
	public CD(String titre, String chanteur, String genre, String compositeur,
			  int anneeParution, int nb_exemplaires, String description, String image){
		super(new IdOeuvre(titre,chanteur),anneeParution, 
				nb_exemplaires,genre, description, image);
		this.compositeur = compositeur; 
	}
	
	public String getCompositeur() {
		return compositeur;
	}
	
	public void setCompositeur(String compositeur){
		this.compositeur= compositeur;
	}
}
