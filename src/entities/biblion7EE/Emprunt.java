package entities.biblion7EE;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.sun.org.apache.xalan.internal.xsltc.dom.BitArray;

@Entity
public class Emprunt implements Serializable {

	@Id
	@Column(name="emprunt_id")
	private Long id;
	
	@OneToOne(mappedBy="emprunt", fetch=FetchType.EAGER)
	private Adherent adherent;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<Oeuvre> liste;
	
	private Integer valide;
	
	private Integer enAttente;
	
	private Long dateDemande;
	
	private Long dateValidation;
	
	public Emprunt() {}
	
	public Emprunt(long id, Set<Oeuvre> liste) {
		this.id = id;
		this.liste = liste;
		this.valide = 0;
		this.enAttente = 0;
		this.dateValidation = 0L;
		this.dateDemande = (new Date()).getTime();
	}


	public Integer getEnAttente() {
		return enAttente;
	}

	public void setEnAttente(Integer enAttente) {
		this.enAttente = enAttente;
	}

	public Long getDateDemande() {
		return dateDemande;
	}

	public void setDateDemande(Long dateDemande) {
		this.dateDemande = dateDemande;
	}

	public Long getDateValidation() {
		return dateValidation;
	}

	public void setDateValidation(Long dateValidation) {
		this.dateValidation = dateValidation;
	}

	public Integer getValide() {
		return valide;
	}

	public void setValide(Integer valide) {
		if (dateValidation == 0) {
			this.dateValidation = (new Date()).getTime();
			this.valide = valide;
		}
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Adherent getAdherent() {
		return adherent;
	}

	public void setAdherent(Adherent adherent) {
		this.adherent = adherent;
	}

	public Set<Oeuvre> getListe() {
		return liste;
	}

	public void setListe(Set<Oeuvre> liste) {
		this.liste = liste;
	}
	
	public void addOeuvres(Set<Oeuvre> oeuvres) {
		for (Oeuvre o : oeuvres) {
			liste.add(o);
		}
	}
	
}
