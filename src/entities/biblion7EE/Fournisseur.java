package entities.biblion7EE;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Fournisseur {

	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private int id;
	
	private String nom;
	
	private String adresse;
	
	private String url;
	
	// Liste des commandes ... on pourrait mettre une classe au dessus de Livre et CD pour 
	// que commande soit bien joli
	
	public Fournisseur() {}

    public Fournisseur(String nom, String adresse, String url) {
	this.nom = nom;
	this.adresse = adresse;
	this.url = url;
    }
}
