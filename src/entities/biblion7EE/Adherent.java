package entities.biblion7EE;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


@Entity
public class Adherent implements Serializable {

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	@Id
	protected String identifiant;
	
	protected String nom;
	
	protected String prenom;
	
	protected String password;

	protected String adresse;

	protected String telephone;

	protected String email;

	protected String dateNaissance;

	protected String dateAdhesion;
	
	  @OneToOne(cascade = javax.persistence.CascadeType.ALL, 
			    fetch = FetchType.EAGER)
	  @JoinColumn(name="emprunt_id")
	protected Emprunt emprunt;

	public Adherent() {}

    public Adherent(String identifiant, String password, 
		    String nom, String prenom,
		    String adresse, String telephone, 
		    String email, String dateNaissance,
		    String dateAdhesion) {

    	this.nom = nom;
    	this.prenom = prenom;
    	this.identifiant = identifiant;
    	this.password = password;
    	this.adresse = adresse;
    	this.telephone = telephone;
    	this.email = email;
    	this.dateNaissance = dateNaissance;
    	this.dateAdhesion = dateAdhesion;
    	this.emprunt = null;
    }

	public Emprunt getEmprunt() {
		return emprunt;
	}

	public void setEmprunt(Emprunt emprunts_livres) {
		this.emprunt = emprunts_livres;
	}

	/**
	 * @return the identifiant
	 */
	public String getIdentifiant() {
		return identifiant;
	}

	/**
	 * @param identifiant the identifiant to set
	 */
	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the adresse
	 */
	public String getAdresse() {
		return adresse;
	}

	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * @param telephone the telephone to set
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the dateNaissance
	 */
	public String getDateNaissance() {
		return dateNaissance;
	}

	/**
	 * @param dateNaissance the dateNaissance to set
	 */
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	/**
	 * @return the dateAdhesion
	 */
	public String getDateAdhesion() {
		return dateAdhesion;
	}

	/**
	 * @param dateAdhesion the dateAdhesion to set
	 */
	public void setDateAdhesion(String dateAdhesion) {
		this.dateAdhesion = dateAdhesion;
	}
}