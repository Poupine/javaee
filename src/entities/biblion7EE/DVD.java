package entities.biblion7EE;

import javax.persistence.Id;
import javax.persistence.Entity;

import biblion7EE.IdOeuvre;


@Entity
public class DVD extends Oeuvre {
	
	private String producteur;
	
	/**
	 * @return the titre
	 */
	public String getTitre() {
		return titre;
	}

	/**
	 * @param titre the titre to set
	 */
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	public String toString() {
		return String.format("titre : %s\nauteur : %s ...", titre, auteur);
	}
	
	public DVD() {}

	public DVD(String titre, String realisateur, 
			   String genre, String producteur,
			   int anneeParution, int nb_exemplaires, 
			   String description, String image) {
		super(new IdOeuvre(titre, realisateur),anneeParution, 
				nb_exemplaires, genre, description, image);
		this.producteur = producteur;
	}

	public String getProducteur() {
		return producteur;
	}

	public void setProducteur(String producteur) {
		this.producteur = producteur;
	}
	
}