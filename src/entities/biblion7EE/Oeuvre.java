package entities.biblion7EE;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import biblion7EE.IdOeuvre;

/*
 * Stratégie par défaut : SINGLE_TABLE : tout dans la même table, sale
 * autres stratégies : JOINED -> jointures, marche bien, TABLE_PER_CLASS -> échec
 */
@Entity
@Inheritance(strategy=InheritanceType.JOINED)
@IdClass(IdOeuvre.class)
public class Oeuvre implements Serializable {

	private static final long serialVersionUID = -920027414104246008L;

	@Column(nullable=false)
	@Id
	protected String auteur;
	
	@Column(nullable=false)
	@Id
	protected String titre;
	
	@Column(nullable=false)
	protected Integer anneeParution;

	@Column(nullable=false)
	protected Integer nb_exemplaires;
	
	@Column(nullable=false)
	protected String genre;
	
	@Column(nullable=false)
	protected Long dateDeCreation; 
	
	@Column(nullable=false, columnDefinition="TEXT")
	protected String description;
	
	// Donne le chemin sur disque de l'image associée à l'oeuvre.
	protected String image;
	
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Oeuvre() {}
	
	public Oeuvre(IdOeuvre id, int annee, int nb_exemplaires, 
				  String genre, String description, String image) {
		this.auteur = id.getAuteur();
		this.titre = id.getTitre();
		this.anneeParution = annee;
		this.nb_exemplaires = nb_exemplaires;
		this.genre = genre;
		this.description = description;
		this.image = image;
		this.dateDeCreation = System.currentTimeMillis();
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Integer getAnneeParution() {
		return anneeParution;
	}

	public void setAnneeParution(Integer anneeParution) {
		this.anneeParution = anneeParution;
	}

	public Integer getNb_exemplaires() {
		return nb_exemplaires;
	}

	public void setNb_exemplaires(Integer nb_exemplaires) {
		this.nb_exemplaires = nb_exemplaires;
	}
	
	public void decExemplaires() {
		this.nb_exemplaires--;
	}
	
	public void incExemplaires() {
		this.nb_exemplaires++;
	}
	
	public Long getTime(){
		return dateDeCreation;
	}
}
