package entities.biblion7EE;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import biblion7EE.IdOeuvre;


@Entity
public class Livre extends Oeuvre {
	
	private String editeur;
	
	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public String getEditeur() {
		return editeur;
	}

	public void setEditeur(String editeur) {
		this.editeur = editeur;
	}

	public Livre(String titre, String auteur, String editeur, 
				 int anneeParution, int nb_exemplaires, String genre, 
				 String description, String image) {
		super(new IdOeuvre(titre, auteur),anneeParution, nb_exemplaires, 
				genre, description, image);
		this.editeur = editeur;
	}
	
	public Livre() {}
	
}
