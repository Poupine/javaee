package entities.biblion7EE;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import biblion7EE.AdminLevel;

@Entity
public class Administrateur extends Adherent implements Serializable {
	
	private AdminLevel niveau;
	
	public Administrateur() {}
	
	public Administrateur(String identifiant, String password, 
		    String nom, String prenom,
		    String adresse, String telephone, 
		    String email, String dateNaissance,
		    String dateAdhesion, AdminLevel niveau) {
		super(identifiant, password, 
			    nom, prenom,
			    adresse, telephone, 
			    email, dateNaissance,
			    dateAdhesion);
		this.niveau = niveau;
	}

	public AdminLevel getNiveau() {
		return niveau;
	}

	public void setNiveau(AdminLevel niveau) {
		this.niveau = niveau;
	}
	
}
