package ejb.biblion7EE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Local;

import entities.biblion7EE.Adherent;
import entities.biblion7EE.Emprunt;
import entities.biblion7EE.Oeuvre;

@Local
public interface BDDEmpruntItf {

	public void validerEmprunt(Emprunt emprunt);
	public void demanderEmprunt(Adherent adh, HashSet<Oeuvre> cart);
	public ArrayList<Emprunt> listeDemande();
	public ArrayList<Emprunt> listeAttente();
	public ArrayList<Emprunt> listeEmpruntsValides();
	public void attenteEmprunt(Emprunt emprunt);
	public void retourEmprunt(Emprunt emprunt);
}
