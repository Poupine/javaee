package ejb.biblion7EE;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import biblion7EE.Md5;
import biblion7EE.StatusAdherent;

import entities.biblion7EE.Adherent;
import entities.biblion7EE.Administrateur;

@Stateless(name="BDDAdherent")
@Local(BDDAdherentItf.class)
public class BDDAdherent implements BDDAdherentItf {

	@PersistenceContext
	private EntityManager em;

	public StatusAdherent ajouterAdherent(Adherent adherent) {
		Adherent adh = em.find(Adherent.class, adherent.getIdentifiant());
		StatusAdherent status = null;
		if (adh != null) {
			status = StatusAdherent.err_identifiant;
		} else {
			if ((status = check(adherent)) == StatusAdherent.adherentOK) {
				adherent.setPassword(Md5.encode(adherent.getPassword()));
				if (adherent instanceof Administrateur) {
					em.persist(((Administrateur)adherent));
				} else {
					em.persist(adherent);
				}
			}
		}
		return status;
	}
	
	private StatusAdherent check(Adherent adherent) {
		Query check_mail = em.createNativeQuery("SELECT * FROM adherent WHERE adherent.email='" + adherent.getEmail() + "'", Adherent.class);
		if (check_mail.getResultList().size() != 0) {
			return StatusAdherent.err_mail;
		}
		return StatusAdherent.adherentOK;
	}

	@Override
	public Adherent findAdherent(String identifiant) {
		Adherent adh = em.find(Adherent.class, identifiant);
		if (adh == null) {
			adh = em.find(Administrateur.class, identifiant);
		}
		return adh;
	}

	@Override
	public void setAdresse(String identifiant, String adresse) {
		Adherent adh = em.find(Adherent.class, identifiant);
		if (adh == null ) {
			adh = em.find(Administrateur.class, identifiant);
		}
		adh.setAdresse(adresse);
		em.merge(adh);
	}

	@Override
	public boolean setPassword(String identifiant, String actuel, String nouveau) {
		Adherent adh = em.find(Adherent.class, identifiant);
		if (adh == null) {
			adh = em.find(Administrateur.class, identifiant);
		}
		if (!Md5.encode(actuel).equals(adh.getPassword())) {
			return false;
		} else {
			adh.setPassword(Md5.encode(nouveau));
			em.merge(adh);
		}
		return true;
	}

}