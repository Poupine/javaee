package ejb.biblion7EE;

import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Stateless;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;

import entities.biblion7EE.Adherent;

@Stateless(name="EmailSender")
@Local(EmailSenderItf.class)
public class EmailSender implements EmailSenderItf {
	
	private Session mailSession;
	private String username;
	private String password;

	@PostConstruct
	public void init() {
		try {
			InitialContext ictx = new InitialContext();
			mailSession = (Session) ictx.lookup("java:/Mail");
			username = mailSession.getProperty("mail.smtp.user");
			password = mailSession.getProperty("mail.smtp.password");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void sendMail(String adresse, String body, String messageType) {
		MimeMessage mail = new MimeMessage(mailSession);
		try {
			mail.setSubject("[Bliblion7EE] Confirmation d'inscription");
			mail.setRecipients(javax.mail.Message.RecipientType.TO,
					 javax.mail.internet.InternetAddress.parse(adresse, false));
			mail.setContent(body, messageType);
			mail.saveChanges();
			Transport transport = mailSession.getTransport("smtp");
			transport.connect(username, password);
			transport.sendMessage(mail, mail.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendMailConfirmation(Adherent adh) {
		StringBuffer mail_content = new StringBuffer();
		mail_content.append("<html><body> Cher " + adh.getPrenom() + ",<br>");
		mail_content.append("Merci d'avoir choisi biblion7ee.");
		mail_content.append("<br> Veuillez trouver ci-dessous le r&eacute;capitulatif de vos identifiants:");
		mail_content.append("<br><br> Login: " + adh.getIdentifiant());
		mail_content.append("<br> Mot de passe: " + adh.getPassword());
		mail_content.append("<br><br> <a href=\"http://localhost:8080/biblion7ee\">Connectez-vous ici</a>");
		mail_content.append("</body></html>");
		sendMail(adh.getEmail(), mail_content.toString(), "text/html");
	}

	@Override
	public void sendMailEmprunt(Adherent adh) {
		StringBuffer mail_content = new StringBuffer();
		mail_content.append("<html><body> Cher " + adh.getPrenom() + ",<br>");
		mail_content.append("Votre sélection d'oeuvre est prête à être retirée.<br>");
		mail_content.append("Vous pouvez venir la récupérer à tout moment pendant les heures d'ouverture.<br>");
		mail_content.append("A très bientôt,<br><br>");
		mail_content.append("L'équipe biblion7ee");
		mail_content.append("</body></html>");
		sendMail(adh.getEmail(), mail_content.toString(), "text/html");
	}
	
	

}
