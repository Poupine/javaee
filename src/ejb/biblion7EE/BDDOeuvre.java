package ejb.biblion7EE;

import java.util.ArrayList;
import java.util.List;

import entities.biblion7EE.DVD;
import entities.biblion7EE.Livre;
import entities.biblion7EE.Oeuvre;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import biblion7EE.IdOeuvre;
import biblion7EE.Recherche;

@Stateless(name="BDDOeuvre")
@Local(BDDOeuvreItf.class)
public class BDDOeuvre implements BDDOeuvreItf {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Boolean ajouter(Oeuvre oeuvre) {
		if (em.find(Oeuvre.class, new IdOeuvre(oeuvre.getTitre(), oeuvre.getAuteur())) != null) {
			return false;
		} else {
			em.persist(oeuvre);
			return true;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Oeuvre> recherche_rapide(String recherche) {
		// FIXME améliorer les résultats
		try {
			recherche = recherche.toLowerCase();
			Query q = em.createQuery(String.format("select oeuvre from Oeuvre oeuvre where LOWER(oeuvre.auteur) like '%%%s%%' or LOWER(oeuvre.genre) like '%%%s%%' or LOWER(oeuvre.titre) like '%%%s%%'",recherche, recherche, recherche));
			return (ArrayList<Oeuvre>)q.getResultList();
		}
		catch (Exception e) {
			return new ArrayList<Oeuvre>();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Oeuvre> recherche_avancee(String categorie, String titre, String auteur, String parution, String genre) {
		String obj = "Oeuvre";
		if (categorie.equals("dvd"))
			obj = "DVD";
		else if (categorie.equals("livre"))
			obj = "Livre";
		else if (categorie.equals("cd"))
			obj = "CD";
		//		System.out.println(obj);
		String query = String.format("select oeuvre from %s oeuvre", obj);
		String[] names = {"titre", "auteur", "genre", "parution"};
		String[] values = {titre.toLowerCase(), auteur.toLowerCase(), genre.toLowerCase(), parution.toLowerCase()};
		query += Recherche.query_builder(names, values);
		Query q = em.createQuery(query);
		return (ArrayList<Oeuvre>)q.getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Oeuvre> newOeuvre() {
		Query q = em.createQuery("SELECT oeuvre FROM Oeuvre oeuvre ORDER BY oeuvre.dateDeCreation DESC");
		ArrayList<Oeuvre> l = (ArrayList<Oeuvre>)q.getResultList();;
		if (l.size()>10){
			l=(ArrayList<Oeuvre>) l.subList(0, 10);
		}
		return l;
	}

}

