package ejb.biblion7EE;

import javax.ejb.Local;

import biblion7EE.StatusAdherent;

import entities.biblion7EE.Adherent;

@Local
public interface BDDAdherentItf {

	public StatusAdherent ajouterAdherent(Adherent adherent);
	public Adherent findAdherent(String identifiant);
	public void setAdresse(String identifiant, String adresse);
	public boolean setPassword(String identifiant, String actuel, String nouveau);
	
}
