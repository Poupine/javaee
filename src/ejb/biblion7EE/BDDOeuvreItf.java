package ejb.biblion7EE;


import java.util.List;

import javax.ejb.Local;

import entities.biblion7EE.DVD;
import entities.biblion7EE.Oeuvre;


@Local
public interface BDDOeuvreItf {
	public List<Oeuvre> recherche_rapide(String recherche);
	public Boolean ajouter(Oeuvre oeuvre);
	public List<Oeuvre> recherche_avancee(String categorie, String titre, String auteur, String genre, String parution);
	public List<Oeuvre> newOeuvre();
	
}
