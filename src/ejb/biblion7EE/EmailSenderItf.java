package ejb.biblion7EE;

import javax.ejb.Local;
import javax.ejb.Remote;

import entities.biblion7EE.Adherent;

@Local
public interface EmailSenderItf {

	public void sendMail(String adress, String body, String messageType);
	public void sendMailConfirmation(Adherent adh);
	public void sendMailEmprunt(Adherent adh);
	
}
