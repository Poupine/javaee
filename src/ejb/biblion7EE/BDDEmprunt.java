package ejb.biblion7EE;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import antlr.collections.List;
import biblion7EE.IdOeuvre;

import entities.biblion7EE.Adherent;
import entities.biblion7EE.Emprunt;
import entities.biblion7EE.Oeuvre;

@Stateless(name="BDDEmprunt")
@Local(BDDEmpruntItf.class)
public class BDDEmprunt implements BDDEmpruntItf {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void validerEmprunt(Emprunt emprunt) {
		emprunt.setValide(1);
		emprunt.setEnAttente(0);
		em.merge(emprunt);
	}

	@Override
	public void demanderEmprunt(Adherent adh, HashSet<Oeuvre> cart) {
		HashSet<Oeuvre> toAdd = new HashSet<Oeuvre>();
		Emprunt emprunt = null;
		for (Oeuvre o : cart) {
			Oeuvre oeuvre = em.find(Oeuvre.class, new IdOeuvre(o.getTitre(), o.getAuteur()));
			oeuvre.decExemplaires();
			em.merge(oeuvre);
			toAdd.add(oeuvre);
		}
		if (adh.getEmprunt() == null) {
			emprunt = new Emprunt(generateId(), toAdd);
			em.persist(emprunt);
			adh.setEmprunt(emprunt);
		} else {
			emprunt = em.find(Emprunt.class, adh.getEmprunt().getId());
			emprunt.addOeuvres(toAdd);
			em.merge(emprunt);
			adh.setEmprunt(emprunt);
		}
		em.merge(adh);
	}

	private long generateId() {
		Query query = em.createNativeQuery("SELECT * FROM emprunt ORDER BY emprunt.emprunt_id DESC", Emprunt.class);
		if (query.getResultList().size() == 0) {
			return 1;
		} else {
			return ((Emprunt)query.getResultList().get(0)).getId() + 1;
		}
	}

	@Override
	public ArrayList<Emprunt> listeDemande() {
		Query query = em.createNativeQuery("SELECT * FROM emprunt WHERE emprunt.valide='0' AND emprunt.enAttente='0' ORDER BY emprunt.datedemande ASC", Emprunt.class);
		return (ArrayList<Emprunt>)query.getResultList();
	}

	@Override
	public void attenteEmprunt(Emprunt emprunt) {
		emprunt.setEnAttente(1);
		em.merge(emprunt);
	}

	@Override
	public ArrayList<Emprunt> listeAttente() {
		Query query = em.createNativeQuery("SELECT * FROM emprunt WHERE emprunt.valide='0' AND emprunt.enAttente='1' ORDER BY emprunt.datedemande ASC", Emprunt.class);
		return (ArrayList<Emprunt>)query.getResultList();
	}

	@Override
	public ArrayList<Emprunt> listeEmpruntsValides() {
		Query query = em.createNativeQuery("SELECT * FROM emprunt WHERE emprunt.valide='1' AND emprunt.enAttente='0' ORDER BY emprunt.datedemande ASC", Emprunt.class);
		return (ArrayList<Emprunt>)query.getResultList();
	}

	@Override
	public void retourEmprunt(Emprunt emprunt) {
		emprunt.getAdherent().setEmprunt(null);
		for (Oeuvre o : emprunt.getListe()) {
			Oeuvre oeuvre = em.find(Oeuvre.class, new IdOeuvre(o.getTitre(), o.getAuteur()));
			oeuvre.incExemplaires();
			em.merge(oeuvre);
		}
		em.remove(em.find(Emprunt.class, emprunt.getId()));
		em.merge(emprunt.getAdherent());
	}
	
}
