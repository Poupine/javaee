package biblion7EE;

public class Parser {

	public static final String localAddress = "biblion7ee";

	
	public static String parseReferer(String referer) {
		String result = "";
		try {
			result = referer.split(localAddress)[1];
			if (result.equals("/")) {
				result = "";
			} else {
				if (!result.contains("?") || result.toLowerCase().contains("affichage")) {
					result = result.split("/")[1];
				} else {
					result = "";
				}
			}
		} catch (Exception e) {
			result = "";
		}
		return result;
	}
	
}
