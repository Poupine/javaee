package biblion7EE;

public class Recherche {
	
	public static String query_builder(String[] names, String[] values) {
		int nb_cond = 0;
		StringBuilder query = new StringBuilder();
		for (int i=0; i<names.length; i++) {
			if (!values[i].equals("")) {
				if (nb_cond > 0) {
					query.append(" and ");
				} else {
					query.append(" where ");
				}
				nb_cond ++;
				query.append(String.format("LOWER(%s) like '%%%s%%'", names[i], values[i]));
			}
			
		}
		return query.toString();
	}

}
