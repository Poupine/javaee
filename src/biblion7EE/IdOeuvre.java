package biblion7EE;

import java.io.Serializable;


public class IdOeuvre implements Serializable {

	private static final long serialVersionUID = -5721386927554358586L;

	private String titre;

	private String auteur;

	public IdOeuvre() {}

	public IdOeuvre(String titre, String auteur) {
		this.titre = titre;
		this.auteur = auteur;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getAuteur() {
		return auteur;
	}

	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public boolean equals(Object obj) {
		boolean resultat = false;

		if (obj == this) {
			resultat = true;
		} else {
			if (!(obj instanceof IdOeuvre)) {
				resultat = false;
			} else {
				IdOeuvre autre = (IdOeuvre) obj;
				if (!titre.equals(autre.titre)) {
					resultat = false;
				} else {
					if (!auteur.equals(autre.auteur)) {
						resultat = false;
					} else {
						resultat = true;
					}
				}
			}
		}
		return resultat;
	}

}
